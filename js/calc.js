let casilla = document.getElementById("operac");
let operacion = 0;
console.log(casilla);

$('button.btn').on('click', function (e) {
    if (casilla.innerHTML == 0) {
        switch (this.id) {
            case "uno":
                operacion = 1;
                casilla.innerHTML = operacion;
                break;
            case "dos":
                operacion = 2;
                casilla.innerHTML = operacion;
                break;
            case "tre":
                operacion = 3;
                casilla.innerHTML = operacion;
                break;
            case "cua":
                operacion = 4;
                casilla.innerHTML = operacion;
                break;
            case "cin":
                operacion = 5;
                casilla.innerHTML = operacion;
                break;
            case "sei":
                operacion = 6;
                casilla.innerHTML = operacion;
                break;
            case "sie":
                operacion = 7;
                casilla.innerHTML = operacion;
                break;
            case "och":
                operacion = 8;
                casilla.innerHTML = operacion;
                break;
            case "nue":
                operacion = 9;
                casilla.innerHTML = operacion;
                break;
            case "sus":
                operacion = "0-";
                casilla.innerHTML = 0;
                break;
            default:
                casilla.innerHTML = 0;
        }

    } else {
        switch (this.id) {
            case "uno":
                operacion = operacion + "1";
                casilla.innerHTML = operacion;
                break;
            case "dos":
                operacion = operacion + "2";
                casilla.innerHTML = operacion;
                break;
            case "tre":
                operacion = operacion + "3";
                casilla.innerHTML = operacion;
                break;
            case "cua":
                operacion = operacion + "4";
                casilla.innerHTML = operacion;
                break;
            case "cin":
                operacion = operacion + "5";
                casilla.innerHTML = operacion;
                break;
            case "sei":
                operacion = operacion + "6";
                casilla.innerHTML = operacion;
                break;
            case "sie":
                operacion = operacion + "7";
                casilla.innerHTML = operacion;
                break;
            case "och":
                operacion = operacion + "8";
                casilla.innerHTML = operacion;
                break;
            case "nue":
                operacion = operacion + "9";
                casilla.innerHTML = operacion;
                break;
            case "cer":
                operacion = operacion + "0";
                casilla.innerHTML = operacion;
                break;
            case "sum":
                operacion = operacion + "+";
                casilla.innerHTML = operacion;
                break;
            case "sus":
                operacion = operacion + "-";
                casilla.innerHTML = operacion;
                break;
            case "div":
                operacion = operacion + "/";
                casilla.innerHTML = operacion;
                break;
            case "mul":
                operacion = operacion + "*";
                casilla.innerHTML = operacion;
                break;
            case "res":
                let resultado = eval(casilla.innerHTML);
                operacion = resultado;
                casilla.innerHTML = resultado;
                break;
            case "lim":
                operacion = 0;
                casilla.innerHTML = operacion;
                break;
        }
    }
})
